module gitlab.com/etke.cc/int/smtp-retry-proxy

go 1.19

require (
	github.com/emersion/go-msgauth v0.6.6
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21
	github.com/emersion/go-smtp v0.15.0
	github.com/getsentry/sentry-go v0.15.0
	github.com/jhillyerd/enmime v0.10.1
	github.com/mileusna/crontab v1.2.0
	gitlab.com/etke.cc/go/env v1.0.0
	gitlab.com/etke.cc/go/healthchecks v1.0.1
	gitlab.com/etke.cc/go/logger v1.1.0
	gitlab.com/etke.cc/go/validator v1.0.6
	go.etcd.io/bbolt v1.3.6
)

require (
	blitiri.com.ar/go/spf v1.5.1 // indirect
	github.com/cention-sany/utf7 v0.0.0-20170124080048-26cad61bd60a // indirect
	github.com/gogs/chardet v0.0.0-20191104214054-4b6791f73a28 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jaytaylor/html2text v0.0.0-20200412013138-3577fbdbcff7 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	gitlab.com/etke.cc/go/trysmtp v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220926161630-eccd6366d1be // indirect
	golang.org/x/net v0.0.0-20221004154528-8021a29435af // indirect
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec // indirect
	golang.org/x/text v0.3.7 // indirect
)
