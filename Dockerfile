FROM registry.gitlab.com/etke.cc/base/build AS builder

WORKDIR /smtp-retry-proxy
COPY . .
RUN make build

FROM registry.gitlab.com/etke.cc/base/app

ENV SMTP_DB_DSN /data/smtp-retry-proxy.db

COPY --from=builder /smtp-retry-proxy/smtp-retry-proxy /bin/smtp-retry-proxy

USER app

ENTRYPOINT ["/bin/smtp-retry-proxy"]
