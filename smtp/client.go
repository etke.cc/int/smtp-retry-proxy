package smtp

import (
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
)

func newClient(backends []string, username, password string) (*smtp.Client, error) {
	var err error
	var client *smtp.Client
	for _, backend := range backends {
		client, err = smtp.Dial(backend)
	}
	if err != nil {
		return nil, err
	}

	err = client.Hello("smtp-retry-proxy")
	if err != nil {
		client.Close()
		return nil, err
	}

	if ok, _ := client.Extension("STARTTLS"); ok {
		client.StartTLS(nil) //nolint:errcheck // we can't do anything here
	}

	if username != "" || password != "" {
		err = client.Auth(sasl.NewPlainClient("", username, password))
		if err != nil {
			client.Close()
			return nil, err
		}
	}

	return client, nil
}
