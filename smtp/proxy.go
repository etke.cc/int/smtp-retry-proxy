package smtp

import (
	"time"

	"github.com/emersion/go-smtp"
	"gitlab.com/etke.cc/go/logger"
	"gitlab.com/etke.cc/go/validator"
	"gitlab.com/etke.cc/int/smtp-retry-proxy/queue"
)

// proxy is smtp proxy
type proxy struct {
	tarpit  time.Duration
	enforce validator.Enforce
	dkim    bool

	log     *logger.Logger
	ping    func() bool
	send    func(*queue.Item) error
	enqueue func(*queue.Item) error
}

func (p *proxy) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	return NewSession(
		state.RemoteAddr,
		username,
		password,
		p.tarpit,
		p.enforce,
		p.dkim,
		p.ping,
		p.send,
		p.enqueue,
		p.log,
	), nil
}

func (p *proxy) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	return NewSession(
		state.RemoteAddr,
		"",
		"",
		p.tarpit,
		p.enforce,
		p.dkim,
		p.ping,
		p.send,
		p.enqueue,
		p.log,
	), nil
}
