package smtp

import (
	"crypto/tls"
	"net"
	"os"
	"time"

	"github.com/emersion/go-smtp"
	"gitlab.com/etke.cc/go/logger"
	"gitlab.com/etke.cc/go/validator"
	"gitlab.com/etke.cc/int/smtp-retry-proxy/queue"
)

// Manager of SMTP proxy
type Manager struct {
	backends []string
	smtp     *smtp.Server
	log      *logger.Logger
	q        *queue.Queue
}

// New proxy manager
func New(
	backends []string,
	tarpit time.Duration,
	enforce validator.Enforce,
	dkim bool,
	q *queue.Queue,
	maxSize int,
	tlsCerts,
	tlsKeys []string,
	log *logger.Logger,
) *Manager {
	m := &Manager{
		backends: backends,
		log:      log,
		q:        q,
	}
	q.SetPing(m.ping)
	q.SetSendmail(m.send)
	m.smtp = smtp.NewServer(&proxy{
		enforce: enforce,
		tarpit:  tarpit,
		dkim:    dkim,
		log:     log,
		ping:    m.ping,
		send:    m.send,
		enqueue: m.q.Add,
	})
	m.smtp.TLSConfig = m.loadTLS(tlsCerts, tlsKeys)
	m.smtp.ReadTimeout = 10 * time.Second
	m.smtp.WriteTimeout = 10 * time.Second
	m.smtp.MaxMessageBytes = maxSize * 1024 * 1024
	m.smtp.AllowInsecureAuth = true
	if log.GetLevel() == "DEBUG" || log.GetLevel() == "TRACE" {
		m.smtp.Debug = os.Stdout
	}

	return m
}

func (m *Manager) loadTLS(certs, keys []string) *tls.Config {
	if len(certs) == 0 || len(keys) == 0 {
		m.log.Warn("SSL certificates are not provided")
		return nil
	}

	certificates := make([]tls.Certificate, 0, len(certs))
	for i, path := range certs {
		tlsCert, err := tls.LoadX509KeyPair(path, keys[i])
		if err != nil {
			m.log.Error("cannot load SSL certificate: %v", err)
			continue
		}
		certificates = append(certificates, tlsCert)
	}
	if len(certificates) == 0 {
		return nil
	}

	return &tls.Config{Certificates: certificates}
}

// ping backends and return true if at least one of them is alive
func (m *Manager) ping() bool {
	for _, backend := range m.backends {
		conn, err := net.DialTimeout("tcp", backend, 10*time.Second)
		if err == nil {
			conn.Close() //nolint:errcheck // doesn't matter
			m.log.Info("backend %q is alive", backend)
			return true
		}
		m.log.Info("backend %q is down: %v", backend, err)
	}

	return false
}

func (m *Manager) send(snapshot *queue.Item) error {
	client, err := newClient(m.backends, snapshot.Username, snapshot.Password)
	if err != nil {
		return err
	}
	defer client.Close()

	err = client.Mail(snapshot.MailFrom, &snapshot.MailOpts)
	if err != nil {
		return err
	}
	for _, rcpt := range snapshot.Rcpt {
		err = client.Rcpt(rcpt)
		if err != nil {
			return err
		}
	}
	w, derr := client.Data()
	if derr != nil {
		return derr
	}

	_, err = w.Write(snapshot.Data)
	if err != nil {
		w.Close() //nolint:errcheck // we can't do anything here
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}

	return nil
}

// Start server
func (m *Manager) Start(port string) error {
	m.log.Info("Starting SMTP server on :%s", port)
	l, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return err
	}

	return m.smtp.Serve(l)
}

// Stop the server
func (m *Manager) Stop() {
	err := m.smtp.Close()
	if err != nil {
		m.log.Error("cannot gracefully shutdown SMTP server: %v", err)
	}
}
