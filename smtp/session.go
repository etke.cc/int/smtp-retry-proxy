package smtp

import (
	"bytes"
	"io"
	"mime"
	"net"
	"net/mail"
	"strings"
	"time"

	"github.com/emersion/go-msgauth/dkim"
	"github.com/emersion/go-smtp"
	"github.com/jhillyerd/enmime"
	"gitlab.com/etke.cc/go/logger"
	"gitlab.com/etke.cc/go/validator"
	"gitlab.com/etke.cc/int/smtp-retry-proxy/queue"
)

type Session struct {
	addr    net.Addr
	tarpit  time.Duration
	enforce validator.Enforce
	dkim    bool
	item    *queue.Item
	ping    func() bool
	send    func(*queue.Item) error
	enqueue func(*queue.Item) error
	log     *logger.Logger
}

var ErrInvalid = &smtp.SMTPError{
	Code:         541,
	EnhancedCode: smtp.EnhancedCode{5, 4, 1},
	Message:      "Your message has been detected and labeled as spam. You must ask the recipient to whitelist you.",
}

// NewSession creates new a new SMTP session
func NewSession(
	addr net.Addr,
	username string,
	password string,
	tarpit time.Duration,
	enforce validator.Enforce,
	dkim bool,
	ping func() bool,
	send func(*queue.Item) error,
	enqueue func(*queue.Item) error,
	log *logger.Logger,
) *Session {
	return &Session{
		addr: addr,
		ping: ping,
		send: send,
		item: &queue.Item{
			Rcpt:     []string{},
			Username: username,
			Password: password,
		},
		enqueue: enqueue,

		enforce: enforce,
		tarpit:  tarpit,
		dkim:    dkim,
		log:     log,
	}
}

func (s *Session) Reset() {
	s.item = nil
}

func (s *Session) Mail(from string, opts smtp.MailOptions) error { //nolint:unparam // interface
	time.Sleep(s.tarpit)

	s.item.MailFrom = from
	s.item.MailOpts = opts

	return nil
}

func (s *Session) Rcpt(to string) error { //nolint:unparam // interface
	time.Sleep(s.tarpit)

	s.item.Rcpt = append(s.item.Rcpt, to)

	return nil
}

func (s *Session) Data(r io.Reader) error {
	time.Sleep(s.tarpit)

	p := enmime.NewParser()
	e, err := p.ReadEnvelope(r)
	if err != nil {
		return err
	}
	e.Root.Header.Set("X-Real-Addr", mime.BEncoding.Encode("utf-8", s.addr.String()))
	var data bytes.Buffer
	err = e.Root.Encode(&data)
	if err != nil {
		return err
	}
	s.item.Data = data.Bytes()

	if !s.validate() {
		return ErrInvalid
	}

	if !s.ping() {
		return s.enqueue(s.item)
	}

	return s.send(s.item)
}

func (s *Session) Logout() error {
	time.Sleep(s.tarpit)

	s.item = nil
	return nil
}

func (s *Session) validate() bool {
	if _, err := mail.ParseAddress(s.item.MailFrom); err != nil {
		s.log.Info("MAIL FROM %q is invalid", s.item.MailFrom)
		return false
	}
	for _, to := range s.item.Rcpt {
		if _, err := mail.ParseAddress(to); err != nil {
			s.log.Info("RCPT TO %q is invalid", to)
			return false
		}
	}

	var sender net.IP
	switch addr := s.addr.(type) {
	case *net.TCPAddr:
		sender = addr.IP
	default:
		host, _, _ := net.SplitHostPort(addr.String()) // nolint:errcheck
		sender = net.ParseIP(host)
	}

	valid := validator.
		New(nil, s.enforce, s.item.Rcpt[0], s.log).
		Email(s.item.MailFrom, sender)
	if !valid {
		s.log.Info("email FROM=%q TO=%q is invalid", s.item.MailFrom, strings.Join(s.item.Rcpt, ","))
		return false
	}

	if s.dkim {
		r := bytes.NewReader(s.item.Data)
		results, verr := dkim.Verify(r)
		if verr != nil {
			s.log.Error("cannot verify DKIM: %v", verr)
			return false
		}
		for _, result := range results {
			if result.Err != nil {
				s.log.Info("DKIM verification of %q failed: %v", result.Domain, result.Err)
				return false
			}
		}
	}

	return true
}
