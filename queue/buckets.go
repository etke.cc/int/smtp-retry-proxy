package queue

import "go.etcd.io/bbolt"

var (
	bucketQueue = []byte("queue")
	buckets     = [][]byte{bucketQueue}
)

func initBuckets(db *bbolt.DB) error {
	return db.Update(func(tx *bbolt.Tx) error {
		for _, name := range buckets {
			_, err := tx.CreateBucketIfNotExists(name)
			if err != nil {
				return err
			}
		}
		return nil
	})
}
