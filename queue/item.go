package queue

import (
	"encoding/binary"

	"github.com/emersion/go-smtp"
)

// Item i a snapshot of SMTP commands
type Item struct {
	Username string // Second command: AUTH
	Password string
	MailFrom string // Third command: MAIL
	MailOpts smtp.MailOptions
	Rcpt     []string // Fourth command: RCPT (can be called multiple times)
	Data     []byte   // Fifth command: DATA
}

// seqID returns an 8-byte big endian representation of v.
func seqID(v uint64, err error) ([]byte, error) {
	if err != nil {
		return nil, err
	}

	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, v)
	return b, nil
}
