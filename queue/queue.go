package queue

import (
	"encoding/json"
	"strings"

	"github.com/emersion/go-smtp"
	"gitlab.com/etke.cc/go/logger"
	"go.etcd.io/bbolt"
)

type Queue struct {
	db       *bbolt.DB
	wip      bool
	log      *logger.Logger
	ping     func() bool
	sendmail func(*Item) error
}

// New queue
func New(db *bbolt.DB, log *logger.Logger) (*Queue, error) {
	err := initBuckets(db)
	if err != nil {
		return nil, err
	}

	return &Queue{db: db, log: log}, nil
}

// SetSendmail function
func (q *Queue) SetSendmail(function func(*Item) error) {
	q.sendmail = function
}

// SetPing function
func (q *Queue) SetPing(function func() bool) {
	q.ping = function
}

// Add item to cache
func (q *Queue) Add(item *Item) error {
	return q.db.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket(bucketQueue)
		id, err := seqID(b.NextSequence())
		if err != nil {
			return err
		}

		itemb, err := json.Marshal(item)
		if err != nil {
			return err
		}

		return b.Put(id, itemb)
	})
}

// Process the queue
func (q *Queue) Process() {
	if q.wip {
		q.log.Info("queue processing is already in progress")
		return
	}
	q.wip = true
	q.log.Info("processing the queue...")
	defer func() { q.wip = false }()
	if !q.ping() {
		q.log.Info("no alive backends found, skipping queue processing")
		return
	}
	q.process()
}

func (q *Queue) process() {
	// NOTE: nil returned everywhere in the Batch below,
	// because we don't care if one of the items will fail
	//nolint:errcheck
	q.db.Batch(func(tx *bbolt.Tx) error {
		b := tx.Bucket(bucketQueue)
		return b.ForEach(func(k, v []byte) error {
			if v == nil {
				return q.processEmpty(b, k)
			}
			q.log.Info("processing queue item %v", k)

			item := q.processUnmarshal(k, v)
			if item == nil {
				return nil
			}
			return q.processItem(b, k, item)
		})
	})
}

func (q *Queue) processEmpty(b *bbolt.Bucket, k []byte) error {
	q.log.Warn("queue item %v is empty", k)
	eerr := b.Delete(k)
	if eerr != nil {
		q.log.Error("cannot remove empty queue item %v", k)
	}
	return nil
}

func (q *Queue) processUnmarshal(k, v []byte) *Item {
	var item *Item
	err := json.Unmarshal(v, &item)
	if err != nil {
		q.log.Error("cannot unmarshal queue item %v", k)
		return nil
	}

	return item
}

func (q *Queue) processItem(b *bbolt.Bucket, k []byte, item *Item) error {
	q.log.Debug("trying to re-send item %v: from=%q to=%q", k, item.MailFrom, strings.Join(item.Rcpt, ", "))
	err := q.sendmail(item)
	if err == nil {
		q.log.Info("attempt to re-send queue item %v succeeded, removing item from the queue", k)
		err = b.Delete(k)
		if err != nil {
			q.log.Error("cannot remove item %v from queue: %v", k, err)
		}
		return nil
	}

	smtpErr, ok := err.(*smtp.SMTPError)
	if !ok {
		q.log.Warn("attempt to re-send queue item %v failed due to non-smtp error: %v; item left in queue", k, err)
		return nil
	}

	q.log.Info("attempt to re-send queue item %v succeeded, backend error: %v, removing item from the queue", k, smtpErr)
	err = b.Delete(k)
	if err != nil {
		q.log.Error("cannot remove item %v from queue: %v", k, err)
	}

	return nil
}
