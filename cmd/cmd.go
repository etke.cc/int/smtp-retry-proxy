package main

import (
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/mileusna/crontab"
	"gitlab.com/etke.cc/go/healthchecks"
	"gitlab.com/etke.cc/go/logger"
	"gitlab.com/etke.cc/go/validator"
	"gitlab.com/etke.cc/int/smtp-retry-proxy/config"
	"gitlab.com/etke.cc/int/smtp-retry-proxy/queue"
	"gitlab.com/etke.cc/int/smtp-retry-proxy/smtp"
	"go.etcd.io/bbolt"
)

var (
	q     *queue.Queue
	db    *bbolt.DB
	hc    *healthchecks.Client
	cron  *crontab.Crontab
	smtpm *smtp.Manager
	log   *logger.Logger
)

func main() {
	quit := make(chan struct{})

	cfg := config.New()
	log = logger.New("proxy.", cfg.LogLevel)

	if len(cfg.Backends) == 0 {
		log.Fatal("no backends provided, nothing to do")
	}

	log.Info("#############################")
	log.Info("SMTP-Retry-Proxy")
	log.Info("Backends: %q", strings.Join(cfg.Backends, ", "))
	log.Info("#############################")

	log.Debug("starting internal components...")
	initSentry(cfg)
	initHealthchecks(cfg)
	initDB(cfg.DB.DSN)
	initQueue(cfg)
	initSMTP(cfg)
	initCron()
	initShutdown(quit)
	defer recovery()

	if err := smtpm.Start(cfg.Port); err != nil {
		//nolint:gocritic
		log.Fatal("SMTP server crashed: %v", err)
	}

	<-quit
}

func initSentry(cfg *config.Config) {
	err := sentry.Init(sentry.ClientOptions{
		Dsn:              cfg.Monitoring.SentryDSN,
		AttachStacktrace: true,
		TracesSampleRate: float64(cfg.Monitoring.SentrySampleRate) / 100,
	})
	if err != nil {
		log.Fatal("cannot initialize sentry: %v", err)
	}
}

func initHealthchecks(cfg *config.Config) {
	if cfg.Monitoring.HealthchecksUUID == "" {
		return
	}
	hc = healthchecks.New(cfg.Monitoring.HealthchecksUUID, func(operation string, err error) {
		log.Error("healthchecks operation %q failed: %v", operation, err)
	})
	hc.Start(strings.NewReader("starting SMTP-Retry-Proxy"))
	go hc.Auto(cfg.Monitoring.HealthchecksDuration)
}

func initDB(dsn string) {
	err := compactDB(dsn)
	if err != nil {
		log.Fatal("cannot optimize database: %v", err)
	}

	db, err = bbolt.Open(dsn, 0o600, nil)
	if err != nil {
		log.Fatal("cannot initialize database: %v", err)
	}
}

func initQueue(cfg *config.Config) {
	var err error
	q, err = queue.New(db, logger.New("queue.", cfg.LogLevel))
	if err != nil {
		log.Fatal("cannot init queue: %v", err)
	}
}

func initSMTP(cfg *config.Config) {
	tarpit := time.Duration(cfg.Tarpit) * time.Second
	enforce := validator.Enforce{
		Email: true,
		MX:    cfg.MX,
		SPF:   cfg.SPF,
		SMTP:  cfg.SMTP,
	}
	smtpm = smtp.New(
		cfg.Backends,
		tarpit,
		enforce,
		cfg.DKIM,
		q,
		cfg.MaxSize,
		cfg.TLS.Certs,
		cfg.TLS.Keys,
		logger.New("smtp.", cfg.LogLevel),
	)
}

func initCron() {
	cron = crontab.New()

	err := cron.AddJob("* * * * *", q.Process)
	if err != nil {
		log.Error("cannot start queue processing cronjob: %v", err)
	}
}

func initShutdown(quit chan struct{}) {
	listener := make(chan os.Signal, 1)
	signal.Notify(listener, os.Interrupt, syscall.SIGABRT, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)

	go func() {
		<-listener
		defer close(quit)

		shutdown()
	}()
}

func shutdown() {
	log.Info("Shutting down...")
	cron.Shutdown()
	smtpm.Stop()
	db.Close()

	if hc != nil {
		hc.Shutdown()
		hc.ExitStatus(0, strings.NewReader("shutting down smtp-retry-proxy"))
	}

	sentry.Flush(5 * time.Second)
	log.Info("SMTP-Retry-Proxy has been stopped")
	os.Exit(0)
}

func recovery() {
	defer shutdown()
	err := recover()
	if err != nil {
		sentry.CurrentHub().Recover(err)
	}
}
