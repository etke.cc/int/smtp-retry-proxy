package main

import (
	"os"

	"go.etcd.io/bbolt"
)

const (
	dbNewSuffix = ".new"
	dbBakSuffix = ".bak"
)

func compactDB(path string) error {
	log.Debug("optimizing database...")
	_, err := os.Stat(path)
	if err != nil {
		log.Info("database is not created or cannot be opened yet, nothing to optimize")
		return nil
	}

	pathd := path + dbNewSuffix
	src, err := bbolt.Open(path, 0o600, &bbolt.Options{ReadOnly: true})
	if err != nil {
		log.Error("cannot open source database: %v", err)
		return err
	}
	dst, err := bbolt.Open(pathd, 0o600, nil)
	if err != nil {
		src.Close()
		log.Error("cannot open target database: %v", err)
		return err
	}
	err = bbolt.Compact(dst, src, 65536)
	src.Close()
	dst.Close()
	if err != nil {
		log.Error("cannot compact database: %v", err)
		return nil
	}

	err = os.Remove(path + dbBakSuffix)
	if err != nil {
		log.Warn("cannot remove database backup: %v", err)
	}

	err = os.Rename(path, path+dbBakSuffix)
	if err != nil {
		log.Error("cannot rename old database to backup: %v", err)
		return err
	}

	err = os.Rename(pathd, path)
	if err != nil {
		log.Error("cannot rename new database to the old one: %v", err)
		return err
	}

	return nil
}
