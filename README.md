# smtp-retry-proxy

An SMTP proxy that responds with OK even if backend (target) SMTP server is unreachable.

Based on [emersion/go-smtp-proxy](https://github.com/emersion/go-smtp-proxy)

## Why?

* Sanity checks (email addresses are valid, email size with attachments is lower than on target server, etc)
* Anti-spam checks (MX, SPF, SMTP, DKIM)
* Anti-spam measurements (tarpitting)
* Enhanced stability of backend SMTP server due to local queue on proxy level + offloaded validations.

## config

env vars

* **SMTP_BACKENDS** - space separated list of target severs with ports, eg: `mail.example.com:25 another.example.com:25`
* **SMTP_PORT** - SMTP port to listen for new emails
* **SMTP_MAXSIZE** - max email size (including attachments) in megabytes
* **SMTP_TLS_CERTS** - space separated list of paths to the SSL certificates (chain) of your domains, note that position in the cert list must match the position of the cert's key in the key list
* **SMTP_TLS_KEYS** - space separated list of paths to the SSL certificates' private keys of your domains, note that position on the key list must match the position of cert in the cert list
* **SMTP_TARPIT** - delay in seconds, default: disabled
* **SMTP_MX** - enforce MX check, default: disabled
* **SMTP_SPF** - enforce SPF check, default: disabled
* **SMTP_SMTP** - enforce SMTP check, default: disabled
* **SMTP_DKIM** - enforce DKIM check, default: disabled
* **SMTP_LOGLEVEL** - log level, default: INFO
* **SMTP_DB_DSN** - path to boltdb file (will be created if not exists)
* **SMTP_MONITORING_SENTRY_DSN** - sentry DSN
* **SMTP_MONITORING_SENTRY_RATE** - sentry sample rate, from 0 to 100 (default: 20)
* **SMTP_MONITORING_HEALTHCHECKS_UUID** - healthchecks.io UUID
* **SMTP_MONITORING_HEALTHCHECKS_DURATION** - healthchecks.io duration between pings in seconds (default: 5)
