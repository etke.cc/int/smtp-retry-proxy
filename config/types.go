package config

import "time"

type Config struct {
	// Backends list of backends
	Backends []string
	// Port for SMTP
	Port string
	// MaxSize of an email (including attachments)
	MaxSize int
	// TLS config
	TLS TLS
	// Tarpit in seconds, default: disabled
	Tarpit int
	// MX enforces MX check
	MX bool
	// SPF enforces SPF check
	SPF bool
	// SMTP enforces SMTP check
	SMTP bool
	// DKIM enforces DKIM checks
	DKIM bool
	// LogLevel
	LogLevel string
	// DB config
	DB DB
	// Monitoring config
	Monitoring Monitoring
}

// DB config
type DB struct {
	// DSN is a database connection string
	DSN string
}

// TLS config
type TLS struct {
	Certs []string
	Keys  []string
}

// Monitoring config
type Monitoring struct {
	SentryDSN            string
	SentrySampleRate     int
	HealthchecksUUID     string
	HealthchecksDuration time.Duration
}
