package config

var defaultConfig = &Config{
	Port:     "25",
	LogLevel: "INFO",
	MaxSize:  100,
	DB: DB{
		DSN: "/tmp/smtp-retry-proxy.db",
	},
	Monitoring: Monitoring{
		SentrySampleRate:     20,
		HealthchecksDuration: 5,
	},
}
