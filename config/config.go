package config

import (
	"time"

	"gitlab.com/etke.cc/go/env"
)

const prefix = "smtp"

// New config
func New() *Config {
	env.SetPrefix(prefix)
	cfg := &Config{
		Backends: env.Slice("backends"),
		Port:     env.String("port", defaultConfig.Port),
		MaxSize:  env.Int("maxsize", defaultConfig.MaxSize),
		TLS: TLS{
			Certs: env.Slice("tls.certs"),
			Keys:  env.Slice("tls.keys"),
		},
		Tarpit:   env.Int("tarpit", defaultConfig.Tarpit),
		MX:       env.Bool("mx"),
		SPF:      env.Bool("spf"),
		SMTP:     env.Bool("smtp"),
		DKIM:     env.Bool("dkim"),
		LogLevel: env.String("loglevel", defaultConfig.LogLevel),
		DB: DB{
			DSN: env.String("db.dsn", defaultConfig.DB.DSN),
		},
		Monitoring: Monitoring{
			SentryDSN:            env.String("monitoring.sentry.dsn", ""),
			SentrySampleRate:     env.Int("monitoring.sentry.rate", defaultConfig.Monitoring.SentrySampleRate),
			HealthchecksUUID:     env.String("monitoring.healthchecks.uuid", ""),
			HealthchecksDuration: time.Duration(env.Int("monitoring.healthchecks.duration", int(defaultConfig.Monitoring.HealthchecksDuration))) * time.Second,
		},
	}
	return cfg
}
